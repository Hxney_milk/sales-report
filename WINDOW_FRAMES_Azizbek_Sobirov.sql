WITH DailySales AS (
    SELECT
        t.calendar_week_number,
        s.time_id,
        t.day_name,
        s.amount_sold,
        ROW_NUMBER() OVER (PARTITION BY t.calendar_week_number, t.day_name ORDER BY s.time_id) AS day_row_num
    FROM
        sales s
    JOIN times t ON s.time_id = t.time_id
    WHERE
        t.calendar_year = 1999
        AND t.calendar_week_number IN (49, 50, 51)
)
SELECT
    calendar_week_number,
    time_id,
    day_name,
    amount_sold AS sales,
    SUM(amount_sold) OVER (PARTITION BY calendar_week_number, day_name ORDER BY time_id) AS cum_sum,
    AVG(amount_sold) OVER (
        PARTITION BY calendar_week_number
        ORDER BY time_id
        ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
    ) AS centered_3_day_avg
FROM
    DailySales
WHERE
    day_row_num = 1
ORDER BY
    calendar_week_number,
    time_id,
    day_name;
